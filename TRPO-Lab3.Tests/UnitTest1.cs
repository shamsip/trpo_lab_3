using NUnit.Framework;
using System;
using static TRPO_Lab3.Lib.Test;

namespace Tests
{

    [TestFixture]
    public class Tests
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            const double R = 2;
            const double H = 6;
            const double expected = 75.36;

            var result = GetCylinderArea(R, H);

            Assert.AreEqual(expected, result);

        }
        [Test]
        public void Test2()
        {
            const double R = -2;
            const double H = 6;

            Assert.Throws<ArgumentException>(() => GetCylinderArea(R, H));
        }
    }
}