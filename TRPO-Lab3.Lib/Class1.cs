﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Class1
    {
        
    }
    public class Test
    {
        const double PI = 3.14;

        public static double GetCylinderArea(double R, double H)
        {
            if (R < 0 | H < 0)
            {
                throw new ArgumentException();
            }
            else
            {
                return 2 * PI * R * H;
            }
        }
    }
}
